import DashboardOrg from '../components/organisms/dashboard/DashboardOrg';

const Dashboard = () => {
  return (
    <DashboardOrg />
  )
}

export default Dashboard;