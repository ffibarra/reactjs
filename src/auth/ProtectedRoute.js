import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import cookie from 'js-cookie';

export const ProtectedRoute = (props) => {
  if (cookie.getJSON('userData')) {
    if(props.path==="/login"){
      return <Redirect to="/dashboard" />
    }else{
      return <Route exact path={props.location.pathname} component={props.component} />
    }
  } else {
    if(props.location.pathname!=="/login"){
      return <Redirect to="/login" />
    }else{
      return <Route exact path={props.location.pathname} component={props.component} />
    }
  }
}

