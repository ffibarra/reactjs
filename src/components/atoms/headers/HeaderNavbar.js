import cookie from 'js-cookie';
import ButtonLogout from '../buttons/ButtonLogout';


const HeaderNavbar = () => {
  const user = cookie.getJSON('userData')
  return (
    <div className="py-5 px-4">
      <h6>Hello {user.name.split(' ')[0]}</h6>
      <ButtonLogout/>
    </div>
  )
}

export default HeaderNavbar