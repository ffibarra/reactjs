const Button = ({type, text}) =>{
  return(
    <button type={type} className="btn button">{text}</button>
  )
}
export default Button;