import React from 'react'
import cookies from 'js-cookie';
import { GoogleLogout } from 'react-google-login';
import { withRouter } from 'react-router-dom';

const ButtonLogout = (props) => {

  const responseGoogleSuccess = (response) => {
    cookies.remove('userData');
    props.history.push("/");
  }

  const responseGoogleFailure = (response) => {
    console.log(response);
  }

  return (
    <GoogleLogout
      clientId={"216342734508-p50ette2m09thnjqa2378aghc13m9okg.apps.googleusercontent.com"}
      buttonText="Logout"
      onLogoutSuccess={responseGoogleSuccess}
      onFailure={responseGoogleFailure}
    />
  );
}

export default withRouter(ButtonLogout);