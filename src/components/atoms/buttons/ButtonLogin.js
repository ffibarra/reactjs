import React from 'react'
import cookies from 'js-cookie';
import GoogleLogin from 'react-google-login';
import { withRouter  } from 'react-router-dom';
import {clientId} from '../../../auth/API';
const ButtonLogin = (props) => {
  const responseGoogleSuccess = (response) => {
    cookies.set('userData', {
      name: response.profileObj.name,
      userName: response.profileObj.email,
      email: response.profileObj.email
    });

    props.history.push("/dashboard");
  }

  const responseGoogleFailure = (response) => {
    console.log(response);
  }

  return (
    <GoogleLogin
      clientId={clientId}
      buttonText="Login with Google"
      onSuccess={responseGoogleSuccess}
      onFailure={responseGoogleFailure}
      cookiePolicy={'single_host_origin'}
      render={
        renderProps => (
          <button className="btn button" onClick={renderProps.onClick} disabled={renderProps.disabled}>
            Login with Google
          </button>
        )
      }
    />
  );
}

export default withRouter(ButtonLogin);