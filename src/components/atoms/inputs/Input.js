
const Input = (props) => {
  const {type, id, placeholder, value, handleFieldChange, valid, required} =props;
  const handleChange = (e) =>{
    handleFieldChange(id, e.target.value);
  }
  return (
    <div className="form-group">
      <input type={type} className={(`form-control ${valid}`)} id={id} value={value} placeholder={placeholder} onChange={handleChange} required={required}/>
    </div>
  )
}

export default Input;