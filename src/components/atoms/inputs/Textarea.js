const Textarea = (props) => {
  const { id, placeholder, value, handleFieldChange, required} =props;
  const handleChange = (e) =>{
    handleFieldChange(id, e.target.value);
  }
  return(
    <div className="form-group">
          <textarea className="form-control" id={id} value={value} placeholder={placeholder} onChange={handleChange} required={required}></textarea>
        </div>
  )
}

export default Textarea;