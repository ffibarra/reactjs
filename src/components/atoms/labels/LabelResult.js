const LabelResult = ({entrySearch, info}) => {
  return (
    <div className="my-3 mx-2">
      Your search for {entrySearch} has {info.formattedTotalResults} results
    </div>
  )
}

export default LabelResult;