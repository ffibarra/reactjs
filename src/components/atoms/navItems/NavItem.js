import React from 'react';
import { NavLink } from 'react-router-dom';

const NavItem = ({href, item}) => {

  return (
    <li className="nav-item">
      <NavLink  className="nav-link px-4" to={(`/dashboard${href}`)}>{item}</NavLink>
    </li>
  )
}

export default NavItem;