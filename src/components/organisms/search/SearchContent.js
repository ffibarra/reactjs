import LabelResult from '../../atoms/labels/LabelResult';
import SearchItems from '../../molecules/search/SearchItems';
import {Col, Row} from 'react-bootstrap';
const SearchContent = ({ results, info, entrySearch }) => {
  return (
    <Row className="formSearch">
        <Col lg={10}>
          <LabelResult entrySearch={entrySearch} info={info} />
          <SearchItems results={results}/>
        </Col> 
    </Row>
  )
}

export default SearchContent;