import React, { useState } from 'react';
import SearchInput from '../../molecules/search/SearchInput';
import SearchIndicators from '../../molecules/search/SearchIndicators';
import SearchContent from './SearchContent';
import { key, cx } from "../../../auth/API";
import axios from 'axios';
import { Container, Row, Col } from 'react-bootstrap';

const SearchOrg = () => {
  const [entrySearch, setEntrySearch] = useState("");
  const [results, setResults] = useState([]);
  const [info, setInfo] = useState('');
  const [postsPerPage] = useState(4);
  const [page, setPagina] = useState(0);
  const [Error, setError] = useState(false);

  const paginationGoogle = async () => {
    try {
      const response = await axios.get(
        `https://www.googleapis.com/customsearch/v1?&key=${key}&cx=${cx}&q=${entrySearch}&start=${page}&num=${postsPerPage}`
      );
      if (response) {
        setResults(response.data.items);
        setInfo(response.data.searchInformation);
      }
    } catch (error) {
      console.log(error);
      setError(!Error);
    }
  };
  const handleResult = (resultItems, resultInfo, entrySearch) => {
    setResults(resultItems);
    setInfo(resultInfo);
    setEntrySearch(entrySearch);
    setPagina(0);
  }
  const handleResultError = (showError) => {
    setError(showError);
  }

  const handlePrev = () => {
    setPagina(page - 1);
    paginationGoogle();
  }
  const handleNext = () => {
    setPagina(page + 1);
    paginationGoogle();
  }

  return (
    <Container fluid className="container-search">
      <SearchInput page={page} postsPerPage={postsPerPage} handleResult={handleResult} handleResultError={handleResultError} />
      {results.length > 0 ?
        <>
          <SearchContent results={results} info={info} entrySearch={entrySearch} />
          <SearchIndicators page={page} handlePrev={handlePrev} handleNext={handleNext} results={results} />
        </>
        : Error &&
        <Row className="formSearch"><Col lg={10}>A ocurrido un error ...</Col></Row>}
    </Container>
  )
}

export default SearchOrg;