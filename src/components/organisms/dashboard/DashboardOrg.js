import MainOrg from './MainOrg';
import SidebarMenu from "./SidebarMenu";
import SearchOrg from '../../organisms/search/SearchOrg';
import { withRouter } from 'react-router-dom';
import {Container, Row} from 'react-bootstrap';

import ContactFormMol from '../../molecules/contact/ContactFormMol';



const DashboardOrg = ({ location }) => {
  switch (location.pathname) {
    case "/dashboard":
      return (
        <Container fluid>
          <Row className="container-height">
            <SidebarMenu />
            <MainOrg>
              <Container fluid className="container-home text-center">
                Welcome to my dashboard
              </Container>
            </MainOrg>
          </Row>
        </Container >
      )
    case "/dashboard/search":
      return (
        <Container fluid>
          <Row className="container-height">
            <SidebarMenu />
            <MainOrg>
              <SearchOrg />
            </MainOrg>
          </Row>
        </Container >
      )
    case "/dashboard/contact":
      return (
        <Container fluid>
          <Row className="container-height">
            <SidebarMenu />
            <MainOrg>
              <ContactFormMol />
            </MainOrg>
          </Row>
        </Container >
      )

    default:
      break;
  }

}

export default withRouter(DashboardOrg);