import { Container, Row} from 'react-bootstrap';
const MainOrg = (props) => {
  return (
    <main className="col-sm-9 col-lg-10">
      <Container className="main">
        <Row className="h-100 ">
          {props.children}
        </Row>

      </Container>

    </main>
  )
}

export default MainOrg;