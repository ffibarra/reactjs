import NavMol from '../../molecules/navColumn/NavFlexColum';
import HeaderNavbar from '../../atoms/headers/HeaderNavbar';

const SidebarMenu = () => {
  return (
    <nav id="sidebarMenu" className="col-sm-3 col-lg-2">
      <div className="position-sticky pt-3">
        <HeaderNavbar />
        <NavMol />
      </div>

    </nav>
  )
}

export default SidebarMenu;