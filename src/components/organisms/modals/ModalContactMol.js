import React from 'react';
import ReactDOM from 'react-dom';
import { Modal, Button} from 'react-bootstrap';

const ModalContactMol = ({ show, handleCancelModal, handleSaveModal, formData }) => {

  return ReactDOM.createPortal(
    <Modal show={show} onHide={handleCancelModal}>
      <Modal.Header closeButton>
        <Modal.Title>Contact information</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p><span className="font-weight-bold">First Name:</span>  {formData.firstName}</p>
        <p><span className="font-weight-bold">Last Name:</span>  {formData.lastName}</p>
        <p><span className="font-weight-bold">Email:</span>  {formData.email}</p>
        <p><span className="font-weight-bold">Message:</span>  {formData.firstName}</p>
        <hr></hr>
          Press save to continue or cancel to correct the data!!!
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleCancelModal}>
          Close
          </Button>
        <Button variant="primary" onClick={handleSaveModal}>
          Save Changes
          </Button>
      </Modal.Footer>
    </Modal>,
    document.querySelector('#modal')
  );
}

export default ModalContactMol;

