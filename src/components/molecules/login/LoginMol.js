import React from 'react';
import LabelLogin from '../../atoms/labels/LabelLogin';
import ButtonLogin from '../../atoms/buttons/ButtonLogin';

const LoginMol = () => {
  return (
    <div className="login">
      <div className="container text-center">
        <LabelLogin />
        <ButtonLogin />
      </div>
    </div>
  );
}

export default LoginMol;