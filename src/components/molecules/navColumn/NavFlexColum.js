import React from 'react';
import NavItem from "../../atoms/navItems/NavItem";

const NavMol = ({ item }) => {

  return (
    <ul className="nav flex-column options">
      <NavItem href="" item="home" />
      <NavItem href="/search" item="search" />
      <NavItem href="/contact" item="contact" />
    </ul>
  )
}

export default NavMol;