import React, { useState } from 'react';
import Input from '../../atoms/inputs/Input';
import Textarea from '../../atoms/inputs/Textarea';
import Button from '../../atoms/buttons/Button';
import ModalContactMol from '../../organisms/modals/ModalContactMol';
import { Container, Form } from 'react-bootstrap';

const ContactFormMol = () => {
  const [formFields, setFormFields] = useState({});
  const [firstName, setfirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [emailConfirm, setEmailConfirm] = useState("");
  const [message, setMessage] = useState("");
  const [valid, setValidText] = useState("");
  const [showModal, setShowModal] = useState(false);

  const handleFieldChange = (id, input) => {
    switch (id) {
      case "firstname":
        setfirstName(input);
        break;
      case "lastname":
        setLastName(input);
        break;
      case "email":
        setEmail(input);
        break;
      case "emailconfirm":
        if (email !== input) {
          setValidText("is-invalid");
        } else {
          setValidText("");
        }
        setEmailConfirm(input);
        break;
      case "message":
        setMessage(input);
        break;

      default:
        break;
    }
  }

  const handleSaveModal = () => {
    setShowModal(false);
    setfirstName("");
    setLastName("");
    setEmail("");
    setEmailConfirm("");
    setMessage("");
  }
  const handleCancelModal = () => setShowModal(false);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if(emailConfirm!==email){
      return;
    }
    const inputs = {
      firstName,
      lastName,
      email,
      message
    }
    setFormFields(inputs)
    setShowModal(true);
  }
  return (
    <Container fluid className="container-form align-self-center">
      <ModalContactMol show={showModal} handleSaveModal={handleSaveModal} handleCancelModal={handleCancelModal} formData={formFields} />
      <Form className="col-6 text-center" onSubmit={handleFormSubmit}>
        <Input type="text" id="firstname" placeholder="First Name" handleFieldChange={handleFieldChange} value={firstName} required={true} />
        <Input type="text" id="lastname" placeholder="Last Name" handleFieldChange={handleFieldChange} value={lastName} required={true} />
        <Input type="email" id="email" placeholder="Email" handleFieldChange={handleFieldChange} value={email} required={true} />
        <Input type="email" id="emailconfirm" placeholder="Confirm Email" handleFieldChange={handleFieldChange} value={emailConfirm} valid={valid} required={true} />
        <Textarea id="message" placeholder="Message" handleFieldChange={handleFieldChange} value={message} required={true} />
        <Button type="submit" text="Submit" />

      </Form>
    </Container>
  )
}

export default ContactFormMol;