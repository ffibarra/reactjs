
const SearchItems = ({results}) => {
  return (
    <>
    {
      results.map((result) => (
        <div key={result.cacheId}>
          <div className="items-search ">
            <div className="m-4 d-flex">
              <div className="mx-3">
                <h4>{result.title}</h4>
                <p>{result.snippet}</p>
              </div>

              <div className="align-self-center">
                <a href={result.link} target="_blank" rel="noopener noreferrer" className="btn btn-primary">Open</a>
              </div>
            </div>
          </div>
          <hr className="mt-4 mb-2 w-25" />
        </div>
      ))
    }
    </>
  )
}
export default SearchItems;