import {Row, Col} from 'react-bootstrap';
const SearchIndicators = ({page, handlePrev , handleNext}) =>{
  return (
      <Row className="formSearch">
        <Col xs={10} className="indicators">
          <Col xs={2}> <span onClick={handlePrev}>{page>0?"Previus":""}</span> </Col>
          <Col xs={2}> <span>Page {page +1}</span> </Col>
          <Col xs={2}> <span onClick={handleNext}>Next</span> </Col>
        </Col>
      </Row>
  )
}
export default SearchIndicators;