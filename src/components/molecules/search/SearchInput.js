import React, { useState } from 'react';
import Button from '../../atoms/buttons/Button';
import { key, cx } from "../../../auth/API";
import axios from 'axios';
import { Row } from 'react-bootstrap';

const SearchInput = ({ page, postsPerPage, handleResult, handleResultError }) => {
  const [entrySearch, setEntrySearch] = useState("");

  const searchGoogle = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.get(
        `https://www.googleapis.com/customsearch/v1?&key=${key}&cx=${cx}&q=${entrySearch}&start=${page}&num=${postsPerPage}`
      );
      if (response) {
        handleResult(response.data.items, response.data.searchInformation, entrySearch);
      }
    } catch (error) {
      console.log(error);
      handleResultError(true);
    }
  };
  return (
    <Row className="formSearch">
      <form onSubmit={searchGoogle} className="col-md-10 col-xl-6">
        <div className="form-group d-flex">
          <input type="text" onChange={(e) => setEntrySearch(e.target.value)} value={entrySearch} className="form-control mr-4" id="search" placeholder="Enter a word to search on Google" required />
          <Button text="Submit" type="submit" />
        </div>
      </form>
    </Row>
  )
}

export default SearchInput;