
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import { ProtectedRoute} from './auth/ProtectedRoute';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Redirect exact from="/" to="/login"/>  
          <ProtectedRoute path="/login" component={Login} />
          <ProtectedRoute path="/dashboard" component={Dashboard} />
          <ProtectedRoute path="/dashboard/search" component={Dashboard} />
          <ProtectedRoute path="/dashboard/contect" component={Dashboard} />
          <Route path="*" component={() => "404 Not found"}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
